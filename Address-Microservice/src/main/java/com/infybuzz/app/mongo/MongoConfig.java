package com.infybuzz.app.mongo;

public interface MongoConfig {

	boolean replication = true;
	int connectionsPerHost = 200;

}
