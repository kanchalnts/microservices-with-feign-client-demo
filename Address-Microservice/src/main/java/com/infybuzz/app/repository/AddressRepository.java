package com.infybuzz.app.repository;

import org.mongodb.morphia.Datastore;
import org.mongodb.morphia.query.Query;
import org.springframework.stereotype.Repository;

import com.infybuzz.app.entity.Address;
import com.infybuzz.app.mongo.CorpMongoConfig;

@Repository
public class AddressRepository  {

	/*
	 * @Autowired Datastore datastore;
	 */
	
	
	public void save(Address address) {
		Datastore datastore = CorpMongoConfig.getMongo();
		datastore.save(address) ;		
	}


	public Address findById(long id) {
		Datastore datastore = CorpMongoConfig.getMongo();
		Query<Address> query = datastore.createQuery(Address.class).filter("id", id);
			return query.get();
			
			
		}	
	
	

}
