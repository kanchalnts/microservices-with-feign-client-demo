package com.infybuzz.app.repository;

import org.mongodb.morphia.Datastore;
import org.mongodb.morphia.query.Query;
import org.springframework.stereotype.Repository;

import com.infybuzz.app.entity.Student;
import com.infybuzz.app.mongo.CorpMongoConfig;

@Repository
public class StudentRepository /* extends JpaRepository<Student, Long> */ {

		
	public Student save(Student student) {
		Datastore datastore = CorpMongoConfig.getMongo();
		 datastore.save(student) ;
		 return student;
	}


	public Student findById(long id) {
		Datastore datastore = CorpMongoConfig.getMongo();
		Query<Student> query = datastore.createQuery(Student.class).filter("id", id);
			return query.get();
			
			
		}	
	
}
