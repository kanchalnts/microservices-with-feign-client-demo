package com.infybuzz.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableFeignClients("com.infybuzz.app.feignclient")
//@ComponentScan({"com.infybuzz.controller", "com.infybuzz.service"})
//@EntityScan("com.infybuzz.entity")
//@EnableJpaRepositories("com.infybuzz.repository")
public class StudentServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(StudentServiceApplication.class, args);
	}

}
