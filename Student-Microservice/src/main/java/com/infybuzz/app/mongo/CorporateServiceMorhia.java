package com.infybuzz.app.mongo;

import org.mongodb.morphia.Morphia;

import com.mongodb.MongoClient;

public class CorporateServiceMorhia extends Morphia{

	
	/**
     * It is best to use a Mongo singleton instance here.
     *
     * @param mongoClient the representations of the connection to a MongoDB instance
     * @param dbName      the name of the database
     * @return a Datastore that you can use to interact with MongoDB
     */
    public CorporateServiceImpl createDatastore(final MongoClient mongoClient, final String dbName) {
        return new CorporateServiceImpl(this, mongoClient, dbName);
    }

}
