package com.infybuzz.app.mongo;

import org.mongodb.morphia.Datastore;

/**
 * @author kanchal k
 *
 */
public class CorpMongoConfig {

	private static CorporateServiceImpl mongo;
	private static String mongoHost;
	private static String mongoPort;

	/**
	 * @return the mongo
	 */
	public static Datastore getMongo() {
		if (mongo == null) {
			initilaizeMongo();
		}
		return mongo;
	}

	private static void initilaizeMongo() {
		if (mongoHost == null || mongoHost.trim().isEmpty()) {
			mongoHost = "127.0.0.1";
		}
		if (mongoPort == null || mongoPort.trim().isEmpty()) {
			mongoPort = "27017";
		}
		mongoPort = mongoPort.trim();
		mongoHost = mongoHost.trim();
		mongo = DatabaseConfiguration.initilaizeMongo(mongoHost, mongoPort, "com.infybuzz.app.repository",
				"ADDRESS");
	}

	public static void initMongo(String host, String port) {
		mongoHost = host;
		mongoPort = port;
	}
}
