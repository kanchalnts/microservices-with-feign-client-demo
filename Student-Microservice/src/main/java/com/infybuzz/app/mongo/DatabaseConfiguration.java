package com.infybuzz.app.mongo;

import java.util.ArrayList;
import java.util.List;

import com.mongodb.MongoClient;
import com.mongodb.MongoClientOptions;
import com.mongodb.ReadPreference;
import com.mongodb.ServerAddress;
import com.mongodb.WriteConcern;

public class DatabaseConfiguration {

	public static CorporateServiceImpl initilaizeMongo(String hosts, String port, String daoPackage, String dbname) {

		CorporateServiceImpl mongo;
		final CorporateServiceMorhia morphia = new CorporateServiceMorhia();
		morphia.getMapper().getOptions().setStoreEmpties(true);
		mongo = morphia.createDatastore(makeConnection(hosts, port), dbname);
		morphia.mapPackage(daoPackage);
		mongo.ensureIndexes();
		return mongo;
	}

	private static MongoClient mongoClient;

	public static MongoClient makeConnection(String hosts, String port) {
		if (mongoClient != null) {
			return mongoClient;
		} else {
			mongoClient = makeConnectionPrivate(hosts, port);

			return mongoClient;
		}
	}

	private static MongoClient makeConnectionPrivate(String hosts, String port) {
		MongoClient mongoClient;
		String[] hostSplit = hosts.split(",");
		String[] portSplit = port.split(",");
		if (MongoConfig.replication) {
			MongoClientOptions options = new MongoClientOptions.Builder().writeConcern(WriteConcern.SAFE)
					.connectionsPerHost(MongoConfig.connectionsPerHost).maxConnectionIdleTime(60000)
					.minConnectionsPerHost(10).readPreference(ReadPreference.primaryPreferred()).build();
			List<ServerAddress> mongoinstances = new ArrayList<ServerAddress>();
			for (int i = 0; i < hostSplit.length; i++) {
				mongoinstances.add(new ServerAddress(hostSplit[i], Integer.parseInt(portSplit[i])));
			}
			mongoClient = new MongoClient(mongoinstances, options);
		} else {
			MongoClientOptions options = new MongoClientOptions.Builder()
					.connectionsPerHost(MongoConfig.connectionsPerHost).minConnectionsPerHost(10)
					.maxConnectionIdleTime(60000).writeConcern(WriteConcern.SAFE).build();
			mongoClient = new MongoClient(new ServerAddress(hostSplit[0], Integer.parseInt(portSplit[0])), options);
		}
		return mongoClient;
	}

}