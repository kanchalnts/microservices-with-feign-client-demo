package com.infybuzz.app.feignclient;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import com.infybuzz.app.request.CreateAddressRequest;
import com.infybuzz.app.response.AddressResponse;

@FeignClient(url = "http://localhost:8072/api/address", value = "address-feignClient")
public interface AddressFeign {

	@PostMapping("/createADD")
	public AddressResponse createAddress(@RequestBody CreateAddressRequest createAddressRequest);

	@GetMapping("/getById/{id}")
	public AddressResponse getById(@PathVariable long id);

	@GetMapping("/test")
	public String test();

}
