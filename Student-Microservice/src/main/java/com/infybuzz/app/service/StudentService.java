package com.infybuzz.app.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.infybuzz.app.entity.Student;
import com.infybuzz.app.feignclient.AddressFeign;
import com.infybuzz.app.repository.StudentRepository;
import com.infybuzz.app.request.CreateAddressRequest;
import com.infybuzz.app.request.CreateStudentRequest;
import com.infybuzz.app.response.StudentResponse;

@Service
public class StudentService {

	@Autowired
	StudentRepository studentRepository;

	@Autowired
	AddressFeign addressFeign;

	public StudentResponse createStudent(CreateStudentRequest createStudentRequest) {

		Student student = new Student();

		student.setId(createStudentRequest.getId());
		student.setFirstName(createStudentRequest.getFirstName());
		student.setLastName(createStudentRequest.getLastName());
		student.setEmail(createStudentRequest.getEmail());

		student.setAddressId(createStudentRequest.getAddressId());
		student = studentRepository.save(student);
		/**************/
		
         CreateAddressRequest  createAddressRequest=new CreateAddressRequest();
         createAddressRequest.setId(createStudentRequest.getId());
         createAddressRequest.setStreet(createStudentRequest.getStreet());
         createAddressRequest.setCity(createStudentRequest.getCity());
         
		
		/**************/
		StudentResponse studentResponse = new StudentResponse(student);
		addressFeign.createAddress(createAddressRequest);
		studentResponse.setAddressResponse(addressFeign.getById(student.getAddressId()));

		return studentResponse;
	}

	public StudentResponse getById(long id) {
		Student student = studentRepository.findById(id);

		StudentResponse studentResponse = new StudentResponse(student);
		studentResponse.setAddressResponse(addressFeign.getById(student.getAddressId()));
		return studentResponse;
	}
}
